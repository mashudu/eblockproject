package StepDefination;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.career;

import java.util.concurrent.TimeUnit;

public class Careers {
    WebDriver driver;
    @Given("^i have lauched the website$")
    public void i_have_lauched_the_website() throws Throwable {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.get("https://www.eblocks.co.za/careers#Current-roles.");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @Given("^User is on careers details$")
    public void user_is_on_careers_details() throws Throwable {
        career ca=new career(driver);
         ca.viewmodel();
    }

    @Given("^i select the view your model and apply$")
    public void i_select_the_view_your_model_and_apply() throws Throwable {
        career ca=new career(driver);
       ca.choose();
       ca.Appply();
    }

    @When("^entering \"([^\"]*)\"and \"([^\"]*)\" and \"([^\"]*)\" a nd \"([^\"]*)\"and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"and (\\d+)$")
    public void entering_and_and_a_nd_and_and_and_and(String FirstName, String LastName, String Email, String Phone, String Adress, String city, String province, int Code) throws Throwable {
        career ca=new career(driver);
        ca.FirstName(FirstName);
        ca.Lastname(LastName);
        ca.email(Email);
        ca.phone(Phone);
        ca.Adress(Adress);
        ca.city(city);
        ca.Province(province);
        ca.code(Code);

    }

    @Then("^i should see result been display$")
    public void i_should_see_result_been_display() throws Throwable {

        career ca=new career(driver);
     // ca.Upload();
        ca.submit();
        ca.ShowResult();
    }


}
