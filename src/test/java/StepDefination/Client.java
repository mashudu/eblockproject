package StepDefination;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import page.clientsPage;

import java.util.concurrent.TimeUnit;


public class Client {
WebDriver driver;
    @Given("^i am presented  the website$")
    public void i_am_presented_the_website() throws Throwable {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.get("https://www.eblocks.co.za/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @When("^i scroll and select image$")
    public void i_scroll_and_select_image() throws Throwable {
        clientsPage  clientsPage=new clientsPage(driver);
        clientsPage.clickImage();
    }

    @Then("^its should display client image$")
    public void its_should_display_client_image() throws Throwable {
        clientsPage  clientsPage=new clientsPage(driver);
        clientsPage.ShowResult();
    }


}
