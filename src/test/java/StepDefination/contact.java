package StepDefination;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.Contact;

import java.util.concurrent.TimeUnit;

public class contact {
    WebDriver driver;

    @Given("^open the browser$")
    public void open_the_browser() throws Throwable {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.get("https://www.eblocks.co.za/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @Given("^User is on contact details$")
    public void user_is_on_contact_details() throws Throwable {
        Contact contact=new Contact(driver);
        contact.clickCareer();
    }

    @When("^entering \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void entering_and_and_and(String username, String email, String Company, String Message) throws Throwable {
        Contact contact=new Contact(driver);
        contact.EnterFullName(username);
        contact.EnterEmail(email);
        contact.EnterCompany(Company);
        contact.EnterMessage(Message);
    }

    @When("^user clicks on submit$")
    public void user_clicks_on_submit() throws Throwable {
        Contact contact=new Contact(driver);
        contact.clickSubmit();
    }

    @Then("^User is navigate to the home page$")
    public void user_is_navigate_to_the_home_page() throws Throwable {
        Contact contact=new Contact(driver);
        contact.CloseBrowser();
    }

}
