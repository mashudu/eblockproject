package StepDefination;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.SelectServices;

import java.util.concurrent.TimeUnit;

public class services {
WebDriver driver;


    @Given("^i have home page$")
    public void i_have_home_page() throws Throwable {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.get("https://www.eblocks.co.za/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @Given("^I am present with home page$")
    public void i_am_present_with_home_page() throws Throwable {
        SelectServices selectServices = new SelectServices(driver);
        selectServices.Services();
    }
    @When("^i select service$")
    public void i_select_service() throws Throwable {

    }

    @When("^i click Agile development$")
    public void i_click_Agile_development() throws Throwable {
        SelectServices selectServices = new SelectServices(driver);
        selectServices.selectser();
    }

    @Then("^i should see Agile Software Delivery and design$")
    public void i_should_see_Agile_Software_Delivery_and_design() throws Throwable {
      SelectServices selectServices=new SelectServices(driver);
      selectServices.ShowResult();

    }
}
