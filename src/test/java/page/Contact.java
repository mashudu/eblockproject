package page;

import Report.reportClass;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Contact {

    WebDriver driver;

    By click_career = By.xpath("/html/body/div[2]/div[1]/nav/a[4]");
    By txt_fullname = By.xpath("/html/body/div[2]/div/div/div[2]/div/form/input[1]");
    By txt_emailAdress = By.xpath("/html/body/div[2]/div/div/div[2]/div/form/input[2]");
    By txt_company = By.xpath("/html/body/div[2]/div/div/div[2]/div/form/input[3]");
    By txt_message = By.xpath("/html/body/div[2]/div/div/div[2]/div/form/input[4]");
    By txt_Submit = By.xpath("/html/body/div[2]/div/div/div[2]/div/form/input[5]");
    By txt_SucessfullyMessage = By.xpath("/html/body/div[2]/div/div/div[2]/div/div[1]/div");

    public  Contact(WebDriver driver) {

        this.driver=driver;

    }

    public void clickCareer(){

        driver.findElement(click_career).click();
    }

    public void EnterFullName(String fullname)
    {
        driver.findElement(txt_fullname).clear();
        driver.findElement(txt_fullname).sendKeys(fullname);
    }

    public void EnterEmail(String email)
    {
        driver.findElement(txt_emailAdress).clear();
        driver.findElement(txt_emailAdress).sendKeys(email);
    }

    public void EnterCompany(String company)
    {
        driver.findElement(txt_company).clear();
        driver.findElement(txt_company).sendKeys(company);
    }
    public void EnterMessage(String message){
        driver.findElement(txt_message).clear();
        driver.findElement(txt_message).sendKeys(message);
    }

    public void clickSubmit( ) throws IOException {

        driver.findElement(txt_Submit).click();
        reportClass re=new reportClass();
        String Text = driver.findElement(txt_SucessfullyMessage).getText();


        ExtentReports extent = re.extentReport();

        ExtentTest test = re.createTest(extent);

        String screenshot = re.takeScreenShot(driver);

        test.log(Status.PASS, "pass");
        test.pass("pass");
        test.pass("Sreenshot for contact", MediaEntityBuilder.createScreenCaptureFromPath( screenshot).build());


        extent.flush();



    }
    public  void CloseBrowser(){
        driver.quit();
    }

}

