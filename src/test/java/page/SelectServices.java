package page;

import Report.reportClass;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SelectServices {
    WebDriver driver;
    By txt_Services=By.xpath("/html/body/div[6]/a");
    By txt_ServicesProvider=By.xpath("/html/body/div[2]/div[2]/a[1]/h3");

    public  SelectServices(WebDriver driver){

        this.driver=driver;
    }

    public void Services() {

        driver.findElement(txt_Services).click();
    }
    public void selectser() throws IOException {
        driver.findElement(txt_ServicesProvider).click();
        reportClass re=new reportClass();
        ExtentReports extent = re.extentReport();

        ExtentTest test = re.createTest(extent);

        String screenshot = re.takeScreenShot(driver);





        test.log(Status.PASS, "pass");
        test.pass("pass");
        test.pass("Sreenshot For Click", MediaEntityBuilder.createScreenCaptureFromPath(screenshot).build());


        extent.flush();
    }
    public void ShowResult(){
        driver.quit();

    }
}
