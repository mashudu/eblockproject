package page;

import Report.reportClass;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class career {

    WebDriver driver;

    By txt_ViewModel=By.xpath("/html/body/div[2]/div[4]/a");
    By txt_Choose=By.xpath("/html/body/section[1]/div[2]/div/div/div/div[1]/div/ul/li[2]/ul/li[2]/a");
    By txt_ButtonApply=By.xpath("/html/body/div[2]/div[3]/div/div/button");
    By txt_FirstName=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[1]/div[1]/div/input");
    By txt_LastName=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[1]/div[2]/div/input");
    By txt_Email=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[2]/div/div/input");
    By txt_Phone=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[3]/div/div/input");
    By txt_Adress=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[4]/div/div/input");
    By txt_city=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[5]/div[1]/div/input");
    By txt_Provide=By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[5]/div[2]/div/input");
    By txt_code =By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[1]/div[5]/div[3]/div/input");

    public  career(WebDriver driver) {
        this.driver = driver;
    }

        public void viewmodel(){

        driver.findElement(txt_ViewModel).click();

    }
    public void choose(){

        driver.findElement(txt_Choose).click();

    }
    public void Appply(){

        driver.findElement(txt_ButtonApply).click();

    }
    public void FirstName(String FirstName){
       // driver.findElement(txt_FirstName).clear();
        driver.findElement(txt_FirstName).sendKeys(FirstName);

    }
    public void Lastname(String lastName){
        //driver.findElement(txt_LastName).clear();
        driver.findElement(txt_LastName).sendKeys(lastName);

    }
    public void email(String Email){
        //driver.findElement(txt_Email).clear();
        driver.findElement(txt_Email).sendKeys(Email);

    }
    public void phone(String Phone){
      //  driver.findElement(txt_Phone).clear();
        driver.findElement(txt_Phone).sendKeys(Phone);

    }
    public void Adress(String Adress){
       // driver.findElement(txt_Adress).clear();
        driver.findElement(txt_Adress).sendKeys(Adress);

    }
    public void city(String city){
       // driver.findElement(txt_city).clear();
        driver.findElement(txt_city).sendKeys(city);

    }
    public void Province(String province){
      //  driver.findElement(txt_Provide).clear();
        driver.findElement(txt_Provide).sendKeys(province);

    }
    public void code(Integer Code){
     //   driver.findElement(txt_code).clear();
        driver.findElement(txt_code).sendKeys(Code.toString());


    }
    public void Upload(){

        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("document.getElementsByName('resumeFileId')[0].setAttribute('type', 'text');");
        //driver.findElement(By.xpath("//input[@name='resumeFileId']")).clear();
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/div/div[2]/div/div/form/div/fieldset[2]/div[1]/div/div/div/div/button/span")).sendKeys("Downloads .txt");
    }
    public void submit() throws IOException {


        driver.findElement(By.xpath("/html/body/footer/div[2]/div/div[1]/button[1]/span[2]")).click();
        reportClass re=new reportClass();
        ExtentReports extent = re.extentReport();

        ExtentTest test = re.createTest(extent);

        String screenshot = re.takeScreenShot(driver);





        test.log(Status.PASS, "pass");
        test.pass("pass");
        test.pass("Sreenshot For Career", MediaEntityBuilder.createScreenCaptureFromPath(screenshot).build());


        extent.flush();

    }
    public void ShowResult(){

        driver.quit();

    }
}
