Feature:Submit the contact form

  Scenario Outline:Check submit form with valid credentials
    Given  open the browser
    And  User is on contact details
    When  entering "<username>" and "<email>" and "<Company>" and "<Message>"
    And user clicks on submit
    Then  User is navigate to the home page

    Examples:

      | username|email|Company|Message|
      |         |     |       |       |
      |tshimbiluni|tshimbiriam@gmail.com|E block|Automation|